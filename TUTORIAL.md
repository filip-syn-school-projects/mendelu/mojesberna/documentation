# Tutorial

## Struktura dokumentu

Dokumenty v LaTeXu se dělí na dvě hlavní části

1. Preambule
2. Obsah

### Preambule

V preambuli se definují vlastnosti daného dokumentu. Dokumenty zpravidla začínají
deklarací `\documentclass[vlastnosti]{třída}`.

Dále lze v preambuli importovat balíčky s dodatečnou funkcionalitou, předefinovat výrazy a provádět mnoho dalších úprav.

```latex
\usepackage[volba]{balíček} % Import balíčku
```

### Obsah

Obsah dokumentu je specifikovaný v prostředí `document`. Cokoliv je napsáno uvnitř tohoto prostředí se propíše do
výsledného dokumentu. V případě tohoto konkrétního dokumentu je na začátku vnořené další prostředí, které je
nazvané `titlepage`. Toto prostředí definuje vlastní podobu úvodní stránky.

Příklad obsahu

```latex
% ... preambule


\begin{document}
    \begin{titlepage}
        % Obsah titulní stránky
    \end{titlepage}

    % Další obsah

\end{document}
```

## Dělení obsahu

Dokumenty jsou členěny na sekce -> podsekce -> podpodsekce

Nový oddíl dokumentu lze vytvořit pomocí následujících příkazů.

| Oddíl       | Příkaz                              |
|-------------|-------------------------------------|
| sekce       | `\section{Název sekce}`             |
| podsekce    | `\subsection{Název podsekce}`       |
| podpodsekce | `\subsubsection{Název podpodsekce}` |

Tyto oddíly jsou standardně číslované (sekce 1.; podsekce 1.1; podpodsekce 1.1.1). Přidáním hvězdičky (`*`) za název
oddílu lze vypnout použití číslování pro tento konkrétní oddíl. Avšak tuto funkci pro účely této práce nebudeme
používat.

Příklad:

```latex
\section{Číslovaná sekce}
Nějaký text v číslované sekci, který je už braný jako odstavec.

\section*{Něčíslovaná sekce}
Tenhle odstavec už spadá do nečíslované sekce.

```

## Odstavce

Tělo textu lze v TeXu vložit buď prostřednictvím toho, že se napíše do souboru nějaké tělo textu. Překladač toto
neoznačené tělo textu automaticky vyhodnotí jako odstavec textu.
Odstavec lze vytvořit také pomocí příkazu `\paragraph{}`

Příklad

```latex
Tohle je názorná ukázka jak funguje odstavec v LaTeXu.

\paragraph{} I takhle funguje text v TeXu.
```

## Prostředí

Asi si říkáte co je to prostředí a jak do něj vstoupím. Prostředí je blok kódu, ve kterém se vyskytují nějaké speciální
funkce. Existují různá prostředí. Například pro odrážkový seznam, číslovaný seznam, matematické vzorce, kód, obrázky,
tabulky a mnoho dalších. I samotný dokument je prostředí. Do prostředí se vstupuje pomocí následující konstrukce.

```latex
\begin{Název prostředí}

\end{Název prostředí}
```

### Figury

Prostředí s názvem `figure` se používa především pro vkládání obrázků. Obrázky, respektive grafiku, lze do dokumentu
vložit s využitím balíčku `graphicx` a pomocí příkazu `\includegraphics{cesta/k/obrazku}`.

Avšak pro účely splnění všech náležitostí, které jsou spojeny s použitím obrázků ve větších pracích, je potřeba obrázky
zabalit do prostředí `figure`.

Kód pro vložení obrázku vypadá většinou takto:

```latex
\begin{figure}[Pozice]
    \centering
    \label{pic:obrazek}
    \includegraphics[velikost]{obrazek.jpg}
    \caption{Obrázek}
\end{figure}
```

- na začátku deklarace prostředí se nachází parametr `pozice`, ve kterém lze specifikovat, kde v dokumentu by se měl
  obrázek nacházet
    - `h` - řekne TeXu, že asi někde v tomhle místě by měl být obrázek
    - `H` nebo `h!` - přebije standardní chování TeXu, který se snaží najít vhodné místo pro obrázek, který by sice měl
      být tady, ale možná si to lehce upraví podle sebe. Pomocí této direktivy, uživatel TeXu řekne "Obrázek prostě bude
      TADY"
    - existují další možnosti
      umisťování [(více zde)](https://www.overleaf.com/learn/latex/Positioning_images_and_tables#The_figure_environment).

- `\centering` zarovná všechno, co se nachází v tomto prostředí na střed
- `\label{}` přiřadí této figuře značku, pomocí které jde tento obrázek referencovat. (Více níže)
- `\includegraphics[velikost]{obrazek.jpg}` - již známý příkaz, vloží obrázek
    - `velikost` - dá se specifikovat různými způsoby, běžně používaný způsob je např. `width=8cm`
    - `obrazek.jpg` je umístění souboru. TeX umí najít obrázky i bez použití koncovky souboru, takže v tomto případě by
      stačilo pouze `obrazek`.
- `\caption{Obrázek}` Přiřadí titulek k této figuře. V tomto případě bude pod obrázkem titulek "Obrázek 1: Obrázek".

#### Značky a reference

V prostředí pro obrázky byl použit příkaz `\label{}`. Tento mechanismus se používá při odkazování na obrázky v textu.
TeX automaticky čísluje obrázky. Aby tento mechanismus nebyl narušen tím, že bychom v textu museli přepisovat reference
na obrázky. Například "jak je vidět na obrázku 2". Z tohoto důvodu můžeme použít příkaz `\ref{}`.

Příklad s návazností na obrázek výše.

```latex
Jak je vidět,na obrázku \ref{pic:obrazek}, je to opravdu hezký obrázek.
```

Do textu se pomocí reference vloží správné číslo obrázku.

## Další struktury

Na začátku obsahu dokumentace můžeme vidět:

```latex
\tableofcontents
\newpage
	
\listoffigures
\newpage
```

- `\tableofcontents` vloží automaticky vygenerovaný obsah na základě nadpisů (sekcí a podsekcí)
- `\listoffigures` vloží seznam obrázků
- `\newpage` vynutí vložení nové stránky.


