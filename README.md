# Dokumentace k projektu MojeSběrna

## O projektu

Dokumentace k projektu [MojeSběrna](https://bitbucket.org/filipsynek/mojesberna/src/master/), vysázená v sázecím
systému [LaTeX](https://www.latex-project.org/)

## Sestavení projektu

K sestavení máte dvě možnosti

- Vyžít distribuci LaTeXu nainstalovanou na počítači
- Využít některý z online nástrojů

### Online nástroje

- [Overleaf](https://cs.overleaf.com/project) (Vyžaduje registraci)

### Lokální instalace

Pro sestavení můžete využít některou z distribucí LaTeXu.

- [TeXLive](https://docs.fedoraproject.org/en-US/neurofedora/latex/) (Pro Linux)
- [MikTeX](https://miktex.org/download) (Pro Windows)

## Tutorial

Rychlý úvod jak se zachází s LaTeXovým dokumentem je poskytnutý v [tomto](TUTORIAL.md) dokumentu.


